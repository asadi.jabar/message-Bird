### Architecture And Request life cycle

#### Architecture
Since it was considered that framework MUST NOT be used, I have created a very simple Architecture . the only components I used is [Klein](https://github.com/klein/klein.php) as Router.

#### Request Life
1) http request hits the endpoint. for example a POST request to `message.bird.dev/messages`
2) helper functions, routes, proper services with it's dependency will loaded
3) request will be routed to proper controller through router
4) in controller layer, request's data will be sanitized and dispatched into proper Queue (this queue was implemented based on SplQueue class  (**this is very simple queue**)).
5) when request dispatched into the Queue, it will automatically call related function from consumer (in this project consumer is a service).
6) service will do related tasks, and return response (or error)
7) response will be returned to controller. response body and http code will returned to router layer and user can see the response.
8) step 6-7 will repeated until Queue has no item anymore.

#### exception handling
all exceptions/errors handled in all layers. and will propagate to upper controller layer

 