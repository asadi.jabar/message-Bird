### Folder Structure
~~~
├── docs                  // The documentation files
│  
├── helpers               // helper functions
│    
├── public                // entry point of the project
│   ├── index.php
│
├── src                   // The source codes folder
│   ├── Container         // IoC to Inject/Register services
|   ├── Controllers       // implementation of controller
|   ├── Exceptions
│   ├── Routes            // single endpoint defined here
│   ├── Sanitize          // filtering/sanitizing/parsing data 
│   └── Service           // Service layer
│ 
├── tests
~~~
 