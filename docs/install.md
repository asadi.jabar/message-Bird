###  API description

#### Authentication
Simply add an Authorization header, based on [Official Documentation](https://developers.messagebird.com/docs/authentication)

(Example:` Authorization: AccessKey {accessKey}`)

#### Additional header 
- `Content-Type: application/json`
- `Accept: application/json`

#### Restful Methods
POST /messages — create a message 

 
Body format is documented in [MessageBird Message Object Documentation](https://developers.messagebird.com/docs/sms-messaging#the-message-object)
 

#### using curl
you can use curl any way.
Please pay attention `message.bird.dev.url/messages` is considered the enpoint
~~~
curl -XPOST -H 'Content-Type: application/json' -H 'Authorization: AccessKey JokdNei04ucSIN0WX4e00Mx41' -H "Content-type: application/json" -d '{
	 
	"recipients": [31612345678],
	"body": "this is test message",
	"originator": "MessageBird"
}' 'message.bird.dev.url/messages'
~~~
 
#### Installation
I assume you have PHP, Nginx already installed and everything is running on a Debian-like Linux machine. If not, well, anyway you know what to do. :P


1) Add `127.0.0.1 message.bird.dev.url` line to `/etc/hosts` file.
2) run `composer install` in the root of the project
~~~
server {
    server_name  message.bird.dev.url;
    root /path/to/public/folder;

    try_files   $uri /index.php$is_args$args;
    index       index.php;
    location ~ \.php(/|$) {
        fastcgi_pass unix:/path/to/php/socket/file
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include         fastcgi_params;
        fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param   DOCUMENT_ROOT   $realpath_root;
        fastcgi_param   REMOTE_PORT     $remote_port;
        fastcgi_param   SERVER_ADDR     $server_addr;
        fastcgi_param   SERVER_PORT     $server_port;
        fastcgi_param   HTTPS off;
        fastcgi_param   REMOTE_ADDR     $http_x_forwarded_for;
        fastcgi_param   APPLICATION_ENV 'dev';

        fastcgi_read_timeout 600s;
        internal;
    }

    #return 404 for all php files as we do have a front controller
    location ~ \.php$ {
        return 404;
    }
}


~~~