<?php

use App\Exceptions\MessageException;
use App\InteractsWithQueue;
use App\ShouldQueueRequest;


if (!function_exists('dispatch')) {

    /**
     * Add an element to dispatcher handler
     *
     * @param  callable $closure
     * @return mixed
     * @throws MessageException
     */

    function dispatch(callable $closure)
    {
        if (!is_callable($closure)) {
            throw new MessageException("input is not callable", 500);
        }

        $dispatchable = $closure();

        if (!$dispatchable instanceof ShouldQueueRequest) {
            throw new MessageException(" dispatchable should be instance of ShouldQueueRequest", 500);
        }

        return (new InteractsWithQueue())->dispatch($dispatchable);
    }


}

if (!function_exists('getAuth')) {

    /**
     * Add an element to dispatcher handler
     *
     * @param $headers
     * @return mixed
     */

    function getAuth(Traversable $headers)
    {
        foreach ($headers as $key => $value) {
            if('Authorization' == $key)
                return trim(str_replace("AccessKey", "", $value));
        }

        throw new \App\Exceptions\MessageInvalidArgumentException("AccessKey is required");
    }


}