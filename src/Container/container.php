<?php

/**
 * |-----------------------------------------
 * | Container Registration
 * |-----------------------------------------
 * | we can register all services we need here ...
 * |
 * |
 */

require __DIR__ . '/../../helpers/helpers.php';


$route = new \Klein\Klein();

$route->respond(
    function ($request, $response, $service, $app) use ($route) {

        $app->register(
            'MessageController', function () {
                return new \App\Controllers\MessageController();
            }
        );




        // Register Message Bird Service
        // Inject Dependencies into it
        $app->register(
            'SmsService', function () use ($app, $request) {

                $object = new \MessageBird\Objects\Message();

               // dev case : JokdNei04ucSIN0WX4e00Mx41
                $client = new \MessageBird\Client(getAuth($request->headers()));

                return new App\Service\Message\SmsService($client, $object);
            }
        );

    }
);