<?php

namespace App;

class InteractsWithQueue extends \SplQueue
{
    /**
     * Queue a queue-able request object
     *
     * @param ShouldQueueRequest $queueable
     * @return InteractsWithQueue
     */
    public function addToQueue(ShouldQueueRequest $queueable)
    {
        $this->enqueue($queueable);

        return $this;
    }

    /**
     * Dequeue request item from the queue
     *
     * @return mixed|null
     */
    public function releaseFromQueue()
    {
        if (!$this->isEmpty()) {
            return $this->dequeue();
        }

        return null;
    }

    /**
     * Dispatch an item from queue
     *
     * @param  ShouldQueueRequest $request
     * @return ShouldQueueRequest|mixed|null
     */
    public function dispatch(ShouldQueueRequest $request)
    {
        $this->addToQueue($request);
        $item = $this->releaseFromQueue();
        $responseArray = [];

        while (null !== $item) {

            $responseArray[] = $item->send();

            $item = $this->releaseFromQueue();
        };

        return $responseArray;


    }

}