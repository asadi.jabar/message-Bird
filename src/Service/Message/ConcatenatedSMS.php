<?php

namespace App\Service\Message;


trait ConcatenatedSMS
{
    use  UserDataHeader;

    /**
     * @var array body per chunk-ed string 
     */
    protected $bodyArray;

    /**
     * @return array
     */
    public function getBodyArray(): array
    {
        return $this->bodyArray;
    }

    /**
     * @param  array $payload
     * @return UserDataHeader
     * @throws \Exception
     */
    public function split(array $payload)
    {
        $this->bodyArray = str_split($payload['body'], 160);

        return $this->udh(count($this->bodyArray));
    }


}