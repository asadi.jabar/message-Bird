<?php

namespace App\Service\Message;


use App\Exceptions\MessageInvalidArgumentException;

trait UserDataHeader
{
    /**
     * @var string reference number
     */
    protected $referenceNumber = null;

    /**
     * @var string get total number of parts
     */
    protected $totalParts = 0;

    /**
     * @var array part's number in the sequence
     */
    protected $sequenceId = [];

    /**
     * @var string binary message prefix
     */
    protected $prefix = '';

    public function getTotalParts(): string
    {
        return $this->totalParts;
    }

    /**
     * @param  string $totalParts
     * @return UserDataHeader
     * @throws \Exception
     */
    public function setTotalParts(string $totalParts)
    {
        if ($totalParts > 255) {
            throw new MessageInvalidArgumentException("your message is too long", 400);
        }

        $this->totalParts = dechex($totalParts);
        return $this;
    }

    /**
     * set all parameters in udh message
     *
     * @param  $size
     * @return UserDataHeader
     * @throws \Exception
     */
    public function udh($size)
    {
        return $this->setPrefix()
            ->setTotalParts($size)
            ->setReferenceNumber()
            ->setSequenceId($size);
    }

    public function setReferenceNumber()
    {
        if (!$this->referenceNumber) {
            $this->referenceNumber = dechex(mt_rand(0, 255));
        }

        return $this;
    }

    /**
     * @param  string $prefix
     * @return UserDataHeader
     */
    public function setPrefix(string $prefix = null)
    {
        $this->prefix = $prefix ?? "050003";
        return $this;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * TOTAL_PARTS : equal for all
     * SEUQUENCE , incremental piece
     * REF unique identifier for all parts (belong togeter)
     * "05 00 03 REF(00-FF) TOTAL_PARTS(00-FF) SEUQUENCE(00-FF)";
     *
     * @return array list of user data headers for binary sms
     */
    public function __invoke()
    {
        $udhList = [];

        foreach ($this->getSequenceId() as $SequenceId) {
            $udhList [] = $this->prefix . $this->referenceNumber . $this->totalParts . $SequenceId;
        }

        return $udhList;
    }

    /**
     * @return array
     */
    public function getSequenceId(): array
    {
        return $this->sequenceId;
    }

    /**
     * @param  int $size
     * @return UserDataHeader
     */
    public function setSequenceId(int $size)
    {
        $sequence = range(1, $size);

        $this->sequenceId = array_map(
            function ($value) {
                return dechex($value);
            }, $sequence
        );

        return $this;
    }


}
