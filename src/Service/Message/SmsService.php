<?php

namespace App\Service\Message;

use App\Exceptions\MessageInvalidArgumentException;
use App\ShouldQueueRequest;
use MessageBird\Client;
use MessageBird\Objects\Message;

class SmsService implements ShouldQueueRequest
{

    use ConcatenatedSMS;

    /**
     * @var Message
     */
    private $object;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $payload;


    /**
     * SmsService constructor.
     *
     * @param Client $client
     * @param Message $object
     */
    public function __construct(Client $client, Message $object)
    {
        $this->client = $client;

        $this->object = $object;
    }

    /**
     * set internal message object
     *
     * @param  $payload
     * @return mixed|null
     */
    public function create($payload)
    {
        $this->payload = $payload;

        $this->checkPayload();

        foreach ($this->payload as $key => $value) {
            if (property_exists($this->object, $key)) {
                $this->object->{$key} = $value;
            }
        }

        return $this;

    }

    /**
     * @return mixed|null
     * @throws \Exception
     * @throws \MessageBird\Exceptions\HttpException
     * @throws \MessageBird\Exceptions\RequestException
     * @throws \MessageBird\Exceptions\ServerException
     */
    public function send()
    {
        $this->checkPayload();

        if ($this->isBinarySms($this->payload['body'])) {

            // generate udh list
            $userDataHeaders = $this->split($this->payload);
            $udhList = $userDataHeaders();

            // create message object respectively
            $response = [];
            for ($i = 0; $i < count($this->getBodyArray()); $i++) {

                $this->object->setBinarySms($udhList[$i], $this->getBodyArray()[$i]);

                // every response has id, backend need to understand we have a binary message and
                // just one id returned from api
                $response[$i] = clone $this->client->messages->create($this->object);
            }

            // we need to have one response per all binary messages
            // I just return one instance of that
            return $response ?? null;
        }

        $response = $this->client->messages->create($this->object);

        return $response ?? null;
    }

    /**
     * @param  $body
     * @return bool
     */
    public function isBinarySms($body)
    {
        return strlen($body) > 160;
    }


    private function checkPayload()
    {
        if (empty($this->payload)){

            throw new MessageInvalidArgumentException("invalid payload", 400);
        }
    }
}