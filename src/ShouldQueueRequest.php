<?php

namespace App;


interface ShouldQueueRequest
{
    /**
     * send the request to consumer
     *
     * @return mixed
     */
    public function send();

}