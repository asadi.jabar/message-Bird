<?php

namespace App\Sanitize;

use App\Exceptions\{
    MessageRunTimeException
};

/**
 * autoload validator class for controller
 *
 * Trait AutoloadValidator
 *
 * @package App\Validator
 */
trait Autoload
{

    use Sanitizer;


    private $className;

    /**
     * @param  $name
     * @param  $args
     * @return bool
     */
    public function __call($name, $args)
    {

        $reflect = new \ReflectionClass($this);
        $class = $reflect->getShortName();

        $matches = preg_split("/(Controller)/", "$class");
        $this->className = '\\App\\Sanitize\\' . $matches[0] . ucfirst($name);

        if (!class_exists($this->className)) {
            throw new MessageRunTimeException("There is no implementation, given: {$this->className}", 400);
        }

        return call_user_func_array([$this, $name], $args);

    }


}