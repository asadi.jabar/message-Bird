<?php
/**
 * Created by PhpStorm.
 * User: asanbar-pc
 * Date: 8/14/18
 * Time: 2:50 AM
 */

namespace App\Sanitize;

use App\Exceptions\MessageInvalidArgumentException;

class MessageValidate
{
    /**
     * @var \ArrayIterator
     */
    private $iterator;

    /**
     * @var array
     */
    private const REQUIRED_PARAMS = [
        'originator' => 'string',
        'body' => 'string',
        'recipients' => 'array'
    ];

    /**
     * MessageValidator constructor.
     *
     * @param \ArrayIterator $iterator
     */
    public function __construct(\ArrayIterator $iterator)
    {
        $this->iterator = $iterator;
    }


    public function accept()
    {
        $haystack = $this->iterator->getArrayCopy();

        foreach (self::REQUIRED_PARAMS as $needleKey => $needleValue) {


            // mandatory field does not exist
            if (!array_key_exists($needleKey, $haystack) || empty($haystack[$needleKey]) || (null == $haystack[$needleKey])) {
                throw new MessageInvalidArgumentException("{$needleKey} is required", 400);
            }

            // type of mandatory object is not the same
            if (gettype($haystack[$needleKey]) != $needleValue) {
                throw new MessageInvalidArgumentException(
                    "field {$needleKey} need to be type {$needleValue}, given type: "
                    . gettype($haystack[$needleKey], 400)
                );
            }

        }

        return true;
    }

}