<?php
/**
 * Created by PhpStorm.
 * User: asanbar-pc
 * Date: 8/14/18
 * Time: 5:19 PM
 */

namespace App\Sanitize;


trait Sanitizer
{
    /**
     * @param  array $args
     * @return \Iterator mixed
     */
    private function filter($args)
    {
        $args = new \RecursiveArrayIterator($args);

        return new $this->className($args);
    }

    /**
     * @param  array $args
     * @return \InvalidArgumentException|bool
     */
    private function validate($args)
    {
        $args = new \ArrayIterator($args);

        return (new $this->className($args))->accept();
    }

}