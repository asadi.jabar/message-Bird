<?php

namespace App\Sanitize;

/**
 * Class MessageValidator
 *
 * @package App\Validator
 */
class MessageFilter extends \RecursiveFilterIterator
{

    /**
     * Check whether the current element of the iterator is acceptable
     *
     * @return bool true if the current element is acceptable, otherwise false.
     */
    public function accept()
    {
        $iterator = $this->getInnerIterator();

        $iteratorIterator = new \RecursiveIteratorIterator($iterator);

        if (is_string($iteratorIterator->current()) && strlen($iteratorIterator->current()) >= 1) {
            return true;

        } else if (is_array($iteratorIterator->current()) && count($iteratorIterator->current()) >= 1) {
            return true;
        }

        return false;
    }

    /**
     * get an copy of filtered data array
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $payload = [] ;
        foreach ($this as $key => $value) {
            $payload [$key] = $value;
        }

        return $payload;
    }
}