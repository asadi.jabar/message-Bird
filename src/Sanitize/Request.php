<?php

namespace App\Sanitize;

/**
 * Trait RequestData
 *
 * @package App\Validators
 */
trait Request
{

    /**
     * @var string 
     */
    protected $data;

}