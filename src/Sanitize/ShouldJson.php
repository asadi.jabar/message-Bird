<?php

namespace App\Sanitize;
use App\Exceptions\{
    MessageInvalidArgumentException
};

/**
 * Trait ShouldJson
 *
 * @package App\Sanitize
 */
trait ShouldJson
{

    /**
     * @param  $request
     * @return MessageInvalidArgumentException|null
     */
    public function checkIfJsonIsValid($request)
    {
        $this->data = json_decode($request, true);

        $error = json_last_error();

        if ($error == JSON_ERROR_SYNTAX 
            || $error == JSON_ERROR_UNSUPPORTED_TYPE
        ) {

            throw new MessageInvalidArgumentException("Data is not valid json", 400);
        }

        return $this->data;
    }

}