<?php

namespace App\Controllers;

use App\Sanitize\{
    Autoload, Request, ShouldJson
};

class BaseController
{
    use Autoload, ShouldJson, Request;


}