<?php

namespace App\Controllers;

class MessageController extends BaseController
{

    /**
     * @param  $request
     * @param  $app
     * @return mixed
     */
    public function send($request, $app)
    {

        try {

            $this->checkIfJsonIsValid($request);

            $data = $this->sanitize();

            return dispatch(
                function () use ($app, $data) {
                    return $app->SmsService->create($data);
                }
            );

        } catch (\Throwable $exception) {
            return [
                "code" => $exception->getCode(),
                "errors" => true,
                'message' => $exception->getMessage()
            ];

        }

    }

    /**
     * check required parameters and
     * filter invalid data
     */
    private function sanitize()
    {
        $this->validate($this->data);

        $filtered = $this->filter($this->data);

        return $filtered->getArrayCopy();
    }


}