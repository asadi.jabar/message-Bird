<?php

/**
 * |-----------------------------------------
 * | Endpoint Registration
 * |-----------------------------------------
 * | inject the requests to targeted controller
 * |
 * |
 */

$route->respond(
    'POST', '/messages', function ($request, $response, $service, $app) use ($route) {

        $body = $request->body();
        $result = $app->MessageController->send($body, $app);
        return (new \App\Response($result, $route->response()))->get();

    }
);

return $route;