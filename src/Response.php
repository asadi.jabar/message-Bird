<?php

namespace App;

class Response
{

    private $response;

    /**
     * Response constructor.
     *
     * @param $body
     * @param $response
     */
    public function __construct(array $body, \Klein\Response $response)
    {

        if (!empty($body['errors']) && $body['errors']) {
            // MessageBird SDK did not seperation between exceptions codes
            // so I make it 400 by default
            $body['code'] = $body['code'] ? $body['code'] : 400;
            $response->code($body['code']);
        }

        $this->response = $body;


    }

    /**
     * @return mixed
     */
    public function get()
    {
        return json_encode($this->response);
    }

}