# Documentation

## Table of content

- [Project Folder Structure](docs/folder-structure.md)
- [Architecture and Request Life Cycle](docs/request-life-cycle.md)
- [How to run Tests](docs/how-to-run-test.md)
- [API usage description](docs/install.md)

#### Requirement
- PHP 7.2+
- PHPUnit 7+
