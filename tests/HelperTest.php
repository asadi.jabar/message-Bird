<?php

namespace Tests;

use PHPUnit\Framework\TestCase;

class HelperTest extends TestCase
{

    /**
     * @var \MessageBird\Client 
     */
    private $client;

    private $messageObject;

    private $mockClient;

    public function setUp()
    {
        //$this->mockClient = $this->getMockBuilder("\MessageBird\Common\HttpClient")->setConstructorArgs(array("fake.messagebird.dev"))->getMock();
        $this->client = new \MessageBird\Client('JokdNei04ucSIN0WX4e00Mx41', $this->mockClient);
        $this->messageObject = new \MessageBird\Objects\Message();
    }


    /**
     * @expectedException \Error
     */
    public function testHelperIfInputIsNotApproperiate()
    {
        dispatch('');
    }

    /**
     * @expectedException \App\Exceptions\MessageException
     */
    public function testHelperIfInputIsNotInstanceOfShouldQueueRequestInterface()
    {
        dispatch(
            function () {

            }
        );
    }

    /**
     * the result of dispatch should be iterable
     */
    public function testHelperShouldReturnIterable()
    {
        $stub = $this->createMock(\App\Service\Message\SmsService::class);

        $return = dispatch(
            function () use ($stub) {
                return $stub;
            }
        );

        $this->assertTrue(is_iterable($return));
    }

    /**
     * @expectedException  \App\Exceptions\MessageInvalidArgumentException
     */
    public function test_headerShouldContainAccessKey()
    {
        getAuth(new \ArrayIterator());
    }


}