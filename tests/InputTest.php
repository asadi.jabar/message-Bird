<?php

namespace Tests;

use PHPUnit\Framework\TestCase;

class InputTest extends TestCase
{

    protected $router;

    public function setUp()
    {
        parent::setUp();

        $this->router = new \Klein\Klein();
    }

    public function testIfOriginatorNull()
    {
        $data = [
            [
                "recipients" => [31612345671],
                "body" => "this is test5"
            ],
            [
                "recipients" => [31612345672],
                "originator" => "",
                "body" => "this is test4"
            ],
            [
                "recipients" => [31612345672],
                "originator" => null,
                "body" => "this is test4"
            ]
        ];
        foreach ($data as $value) {
            $request = new \App\Controllers\MessageController();
            $response = $request->send(json_encode($value), $this->router);
            $this->assertSame('{"code":400,"errors":true,"message":"originator is required"}', json_encode($response));
        }

    }


    public function testIfBodyNull()
    {
        $data = [
            [
                "recipients" => [31612345674],
                "originator" => "MessageBird3",
                "body" => ""
            ],
            [
                "recipients" => [31612345675],
                "body" => null,
                "originator" => "MessageBird2"
            ],
            [
                "recipients" => [31612345673],
                "originator" => "MessageBird1"
            ],
        ];
        foreach ($data as $value) {
            $request = new \App\Controllers\MessageController();
            $response = $request->send(json_encode($value), $this->router);
            $this->assertSame('{"code":400,"errors":true,"message":"body is required"}', json_encode($response));
        }

    }

    public function testIfRecipientsNull()
    {
        $data = [
            [
                "originator" => "MessageBird7",
                "body" => "this is body"
            ],
            [
                "recipients" => "",
                "body" => "this is body",
                "originator" => "MessageBird8"
            ],
            [
                "body" => "this is body",
                "originator" => "MessageBird9",
                "recipients" => null
            ],
        ];
        foreach ($data as $value) {
            $request = new \App\Controllers\MessageController();
            $response = $request->send(json_encode($value), $this->router);
            $this->assertSame('{"code":400,"errors":true,"message":"recipients is required"}', json_encode($response));
        }

    }


    public function testInvalidBody()
    {
        $data = '{
	     "recipients": [31612345678],
	     "body": "this is test ",
	     "originator": "MessageBird\\"
        }';
        $request = new \App\Controllers\MessageController();
        $response = $request->send(json_encode($data), $this->router);
        $this->assertSame('{"code":0,"errors":true,"message":"Passed variable is not an array or object"}', json_encode($response));

    }

}