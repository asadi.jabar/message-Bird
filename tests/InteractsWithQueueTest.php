<?php

namespace Tests;

use App\InteractsWithQueue;
use PHPUnit\Framework\TestCase;

class InteractsWithQueueTest extends TestCase
{

    /**
     * @return InteractsWithQueue
     */
    public function testCanAddToQueue()
    {
        $handler = new InteractsWithQueue();
        $stub = $this->createMock(\App\Service\Message\SmsService::class);
        $return = $handler->addToQueue($stub);
        $this->assertInstanceOf(\SplQueue::class, $return);
        return $return;
    }

    /**
     * @depends testCanAddToQueue
     */
    public function testPopCanReleaseItemFromQueue($item)
    {
        $this->assertInstanceOf(\App\ShouldQueueRequest::class, $item->releaseFromQueue());

        $this->assertNull($item->releaseFromQueue());
    }
}
