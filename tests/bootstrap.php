<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);

require_once __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../helpers/helpers.php';
require __DIR__ . '/../src/Container/container.php';