<?php

namespace Tests;

use PHPUnit\Framework\TestCase;

class SmsServiceTest extends TestCase
{

    private $client;

    private $messageObject;

    private $mockClient;

    public function setUp()
    {
        parent::setUp();

        //$this->mockClient = $this->getMockBuilder("\MessageBird\Common\HttpClient")->setConstructorArgs(array("fake.messagebird.dev"))->getMock();
        $this->client = new \MessageBird\Client('JokdNei04ucSIN0WX4e00Mx41', $this->mockClient);
        $this->messageObject = new \MessageBird\Objects\Message();
    }

    public function test_AddBody()
    {
        $body = <<<EOT
{
	 
	"recipients": [31612345678],
	"body": "11This is a test message. 22This is a test message 33This is a test message.44 This is a test message. 555This is a test message. 66This is a test message.77 This is a test mesbbbsage. 88This is a test message. 99This is a test message. 10This is a test message.  11This is a test message. 12This is a tbest message. 66This is a test message. 77This is a test message. 88This is a test message. 99This is a test message. 00This is a test message. 11This is a test message.12This is a test message. 13This is a test message. ",
	"originator": "MessageBird"
}
EOT;
        $this->assertNotNull($body);
        return $body;
    }


    public function test_shoudImplementShouldQueueRequest()
    {
        $instance = $this->createMock(\App\Service\Message\SmsService::class);

        $this->assertInstanceOf(\App\ShouldQueueRequest::class, $instance);

    }


    /**
     * @depends test_AddBody
     */
    public function test_CreateFunctionShoudReturnSmsServiceInstace($body)
    {
        $BodyArray = json_decode($body, true);
        $smsServiceInstance = new \App\Service\Message\SmsService(
            $this->client,
            $this->messageObject
        );
        $result = $smsServiceInstance->create($BodyArray);
        $this->assertInstanceOf(\App\Service\Message\SmsService::class, $result);
    }

    /**
     * @depends test_AddBody
     */
    public function test_CheckMandatoryFieldsOnMessageObject($body)
    {
        $BodyArray = json_decode($body, true);
        $smsServiceInstance = new \App\Service\Message\SmsService(
            $this->client,
            $this->messageObject
        );

        $result = $smsServiceInstance->create($BodyArray);

        // access to private method of class
        $MessageObjectContext = function ($propertyName) {
            return $this->object->{$propertyName};
        };

        $MessageObjectContext->bindTo(
            $result,
            $result
        );

        $body = $MessageObjectContext('body');

        $this->assertSame(
            $BodyArray->body,
            $body
        );
    }

    /**
     * @depends test_AddBody
     */
    public function test_isBinarySms($body)
    {
        $BodyArray = json_decode($body, true);

        $smsServiceInstance = new \App\Service\Message\SmsService(
            $this->client,
            $this->messageObject
        );

        $this->assertTrue($smsServiceInstance->isBinarySms($BodyArray['body']));
    }

    /**
     * @expectedException \App\Exceptions\MessageInvalidArgumentException
     */
    public function test_callSendFunctionWithoutSettingData()
    {
        $smsServiceInstance = new \App\Service\Message\SmsService(
            $this->client,
            $this->messageObject
        );

        $smsServiceInstance->send();

        $smsServiceInstance->create();

    }


}