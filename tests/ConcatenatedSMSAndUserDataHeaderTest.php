<?php
/**
 * Created by PhpStorm.
 * User: asanbar-pc
 * Date: 8/17/18
 * Time: 1:57 AM
 */

namespace Tests;

use App\Service\Message\ConcatenatedSMS;
use PHPUnit\Framework\TestCase;

class ConcatenatedSMSAndUserDataHeaderTest extends TestCase
{
    private $concatenatedSMSInstance;


    public function setUp()
    {
        parent::setUp();

        $this->concatenatedSMSInstance = new class()
        {
            use ConcatenatedSMS;
        };
    }

    public function testAddLongBody()
    {
        $longBody = <<<EOT
{
	 
	"recipients": [31612345678],
	"body": "11This is a test message. 22This is a test message 33This is a test message.44 This is a test message. 555This is a test message. 66This is a test message.77 This is a test mesbbbsage. 88This is a test message. 99This is a test message. 10This is a test message.  11This is a test message. 12This is a tbest message. 66This is a test message. 77This is a test message. 88This is a test message. 99This is a test message. 00This is a test message. 11This is a test message.12This is a test message. 13This is a test message. ",
	"originator": "MessageBird"
}
EOT;
        $this->assertNotNull($longBody);
        return $longBody;
    }


    /**
     * @depends testAddLongBody
     */
    public function test_DataWithLongLengthBreakDownIntoSmallerChunck($longBody)
    {
        $longBodyArray = json_decode($longBody, true);

        $splited = $this->concatenatedSMSInstance->split($longBodyArray);

        $this->assertEquals(4, count($splited->getBodyArray()));

    }

    /**
     * @depends testAddLongBody
     */
    public function test_InstanceOfUserDataHeaderShouldBeReturned($longBody)
    {
        $longBodyArray = json_decode($longBody, true);

        $return = $this->concatenatedSMSInstance->split($longBodyArray);

        $this->assertEquals('050003', $return->getPrefix());
    }

    /**
     * @depends testAddLongBody
     */
    public function test_stringAsArgumentIsNotAcceptable($longBody)
    {
        $this->expectException(\Error::class);
        $return = $this->concatenatedSMSInstance->split($longBody);
    }

    /**
     * @depends testAddLongBody
     */
    public function test_UDH_totalPartsCalculation($longBody)
    {
        $longBodyArray = json_decode($longBody, true);
        $return = $this->concatenatedSMSInstance->split($longBodyArray);
        $this->assertEquals(dechex(4), $return->getTotalParts());
    }

    /**
     * @depends testAddLongBody
     */
    public function test_UDH_sequenceIdCalculation($longBody)
    {
        $longBodyArray = json_decode($longBody, true);
        $return = $this->concatenatedSMSInstance->split($longBodyArray);

        $ranges = range(1, count($return->getBodyArray()));
        $sequences = array_map(
            function ($value) {
                return dechex($value);
            }, $ranges
        );

        $this->assertTrue(is_array($return->getSequenceId()));

        $this->assertSame($sequences, $return->getSequenceId());
    }

}