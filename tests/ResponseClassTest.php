<?php

namespace Tests;

use App\Response;
use PHPUnit\Framework\TestCase;

class ResponseTestClass extends TestCase
{
    protected $responseObject;

    protected $mockClient;

    public function setUp()
    {
        parent::setUp();

        $this->responseObject = new \Klein\Response();
    }


    public function testSuccessResponseData()
    {
        $response = <<<EOT
{
	"direction": "mt",
	"type": "binary",
	"originator": "MessageBird",
	"body": "11This is a test message. 22This is a test message 33This is a test message.44 This is a test message. 555This is a test message. 66This is a test message.77 Th",
	"reference": null,
	"validity": null,
	"gateway": 10,
	"typeDetails": {
		"udh": "050003e941"
	},
	"datacoding": "plain",
	"mclass": 1,
	"scheduledDatetime": null,
	"recipients": {
		"totalCount": 1,
		"totalSentCount": 1,
		"totalDeliveredCount": 0,
		"totalDeliveryFailedCount": 0,
		"items": [{
			"recipient": 31612345678,
			"status": "sent",
			"statusDatetime": "2018-08-16T20:31:48+00:00"
		}]
	},
	"reportUrl": null
}
EOT;

        $this->assertJson($response);
        $response = json_decode($response, true);
        return $response;

    }


    /**
     * @depends testSuccessResponseData
     */
    public function testSuccessResponse($response)
    {
        $res = new Response($response, $this->responseObject);

        $this->assertSame(json_encode($response), $res->get());
    }


    public function testBadRequest()
    {
        $response = [
            "code" => 400,
            "errors" => true,
            'message' => "this is simple bad request error"
        ];
        $res = new Response($response, $this->responseObject);
        $badResponse = json_decode($res->get(), true);
        $this->assertEquals(400, $badResponse['code']);
    }

}